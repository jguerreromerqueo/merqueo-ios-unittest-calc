//
//  CalculatorViewControllerTests.swift
//  wallcalcaroniTests
//
//  Created by John Guerrero on 5/15/19.
//  Copyright © 2019 Caleb Stultz. All rights reserved.
//

import XCTest

@testable import wallcalcaroni

class CalculatorViewControllerTests: XCTestCase {

    var calculatorViewController : CalculatorViewController!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        calculatorViewController = storyboard.instantiateViewController(withIdentifier: "CalculatorViewController") as? CalculatorViewController
        calculatorViewController.loadViewIfNeeded()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_summatory(){
        calculatorViewController.fiveBtn.sendActions(for: .touchUpInside)
        calculatorViewController.addBtn.sendActions(for: .touchUpInside)
        calculatorViewController.fiveBtn.sendActions(for: .touchUpInside)
        calculatorViewController.equalsBtn.sendActions(for: .touchUpInside)
        let result = calculatorViewController.valueLabel.text
        XCTAssertEqual(result, "10")
    }

}
