//
//  wallcalcaroniTests.swift
//  wallcalcaroniTests
//
//  Created by John Guerrero on 5/15/19.
//  Copyright © 2019 Caleb Stultz. All rights reserved.
//

import XCTest

@testable import wallcalcaroni

class wallcalcaroniTests: XCTestCase {
    
    
    var calculatorManager:CalculationManager!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        calculatorManager = CalculationManager()
    }

    override func tearDown(){
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        calculatorManager = nil
    }
    
    func test_dividingByZero(){
        calculatorManager.valueA = 5
        calculatorManager.valueB = 0
        calculatorManager.currentOperand = .divide
        XCTAssertTrue(calculatorManager.currentOperand == .divide && calculatorManager.valueB != 0)
//        do {
//            try let result = calculatorManager.calculate()
//            XCTAssertTrue(true, "")
//        } catch {
//            print("Error \(error.localizedDescription)")
//            XCTFail()
//        }
    }
    
    /*TODO:
     * Correct instanciation
     * Dividing by zero
     * Four principal operations
     * Floating numbers
     * Negative values
     * Decimal check
     * what if I try to add a third number
     */

}
